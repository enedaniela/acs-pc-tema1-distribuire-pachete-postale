#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>

typedef struct{
	int id_cartier;
	char * nume_cartier;
}cartier;

typedef struct{
	int id_pachet;
	int adresa[18];
	int id_cartier;
	int strada;
	int numar;
	int prioritate;
	float greutate;
	char * mesaj;
	int codificare_mesaj;
}pachet;

typedef struct{
	int id_cartier;
	int nr_pachete;
	pachet v[50];
}postas;

void citire(cartier *c, pachet *p, int *nrP, int *nrC)
{
	char sir_c[20], sir_p[100];
	int i, j;

	scanf("%d", nrC);
	getchar();

	for(i = 0; i < *nrC; i++)
		c[i].id_cartier = i;
	for(i = 0; i < *nrC; i++)
	{
		gets(sir_c);
		c[i].nume_cartier = (char *) malloc(strlen(sir_c) * sizeof(char));
		strcpy(c[i].nume_cartier, sir_c);

		printf("%d ", c[i].id_cartier);
		printf("%s\n", c[i].nume_cartier);
	}

	scanf("%d", nrP);

	for(i = 0; i < *nrP; i++)
		p[i].id_pachet = i;
	for(i = 0; i < *nrP; i++)
	{
		printf("%d\n", p[i].id_pachet);

		for(j = 0; j < 18; j++)
			scanf("%d", &p[i].adresa[j]);
		for(j = 0; j < 18; j++)
			printf("%d ", p[i].adresa[j]);
		printf("\n");
		scanf("%d", &p[i].prioritate);
		printf("%d ", p[i].prioritate);
		scanf("%f", &p[i].greutate);
		printf("%f ", p[i].greutate);
		getchar();

		gets(sir_p);
		p[i].mesaj = (char *) malloc(strlen(sir_p) * sizeof(char));
		strcpy(p[i].mesaj, sir_p);

		printf("%s\n", p[i].mesaj);
	}
	
}

void adresa( pachet *p, int *nrP)
{	
	int i, j, k;
	for(j = 0; j < *nrP; j++)
	{
		printf("%d ", p[j].id_pachet);

		//se extrage id-ul cartierului
		p[j].id_cartier = 0;
		k = 4;
		for(i = 0; i < 5; i++)
		{
			p[j].id_cartier = p[j].id_cartier + p[j].adresa[i] * pow(2, k);
			k--;
		}
		printf("%d ", p[j].id_cartier);

		//se extrage strada
		p[j].strada = 0;
		k = 4;
		for(i = 5; i < 10; i++)
		{
			p[j].strada+=p[j].adresa[i] * pow(2, k);
			k--;
		}
		printf("%d ", p[j].strada);

		//se extrage numarul
		p[j].numar = 0;
		k = 7;
		for(i = 10; i < 18; i++)
		{
			p[j].numar+=p[j].adresa[i] * pow(2, k);
			k--;
		}
		printf("%d ", p[j].numar);

		printf("\n");
		
	}
		
}

void distribuire_pachete(cartier *c, pachet *p, postas *post, int *nrP, int *nrC)
{
	int i, j, k;

	//initializez id-urile postasilor;
	for(i = 0; i < *nrC; i++)
		post[i].id_cartier = i;

	// initializez nr de pachete al fiecarui postas cu 0;	
	for(i = 0; i < *nrC; i++)
		post[i].nr_pachete = 0;
	
	// distribui pachetele
	for(j = 0; j < *nrC; j++)
	{	
		k = 0;	
		for(i = 0; i < *nrP; i++)
		{
			if(p[i].id_cartier == post[j].id_cartier)
			{
				post[j].v[k] = p[i];
				post[j].nr_pachete++;
				k++;
			}
		}
			
	}
	for(i = 0; i < *nrC; i++)
	{
		printf("%d %d ", post[i].id_cartier, post[i].nr_pachete);

		for(j = 0; j < post[i].nr_pachete; j++)
			printf("%d ", post[i].v[j].id_pachet);
		
		printf("\n");
	}

}

void comparare( postas *post, pachet *p, int *nrP, int *nrC)
{
	int i, j, k;
	pachet aux;

	for(k = 0; k < *nrC; k++)
		// sortare folosind Bubble Sort
		for(i = 1; i < post[k].nr_pachete; i++)
		{
			for(j = 0; j < post[k].nr_pachete - 1; j++)
			{ 
				//sortare in functie de prioritate
				if(post[k].v[j].prioritate < post[k].v[j + 1].prioritate)
				{
					aux = post[k].v[j];
					post[k].v[j] = post[k].v[j + 1];
					post[k].v[j + 1] = aux;
				}
				if(post[k].v[j].prioritate == post[k].v[j + 1].prioritate)

					//sortare in functie de greutate
					if(post[k].v[j].greutate <  post[k].v[j + 1].greutate)
					{
						aux = post[k].v[j];
						post[k].v[j] = post[k].v[j + 1];
						post[k].v[j + 1] = aux;
					}	
			}
		}
				
			for(i = 0; i < *nrC; i++)
			{
				printf("%d %d ", post[i].id_cartier, post[i].nr_pachete);

				for(j = 0; j < post[i].nr_pachete; j++)
					printf("%d ", post[i].v[j].id_pachet);

				printf("\n");
			}
		
}

char* inversare(pachet *p)
{
	char *v, temp[100];
	int i, k, j;

	v = (char *)malloc(100);
		v[0] = 0; //initializare cu sir vid;
		i = strlen(p->mesaj) - 1;
		while(i > 0)
		{
//sar de la final catre inceput caracterele necorespunzatoare

			while(strchr(" .,!?:", p->mesaj[i]))
				i--;
// in acest moment sir[i] are un caracter bun
			j = i;

//sar caracterele bune
			while(strchr(" .,!?:", p->mesaj[i])==0)
				i--;

//sir[i] este primul caracter necorespunzator
			k =i + 1;
			strncpy(temp, p->mesaj + k, j - k + 1);
			temp[j - k + 1] = 0;
			strcat(v, temp);
		}
		
	return v;
}

int cod (pachet *p)
{	
	char * sir;
	int s, i, c, cod = 0;

//initializez cu mesajul rezultate dupa elminarea spatiilor si delimitatorilor
	sir = inversare(p);
	s = strlen(sir);
	for(i = 0; i < s; i++)
	{
		//parcurgand sirul caracter cu caracter, calculez suma dintre produsele valorii ASCII a caracterului si pozitia sa in noul sir
		c = sir[i];
		cod = cod + c * i;
	}
	
	return cod;

}

int verificare(int cod_mesaj, int id_cartier)
{	
	int p = 0, id_cartier_c, r;

	id_cartier_c = id_cartier;
	while(id_cartier_c)
	{
		//calculez numarul de cifre al id-ului postasului
		id_cartier_c = id_cartier_c / 10;
		p++;
	}
	if(id_cartier == 0)
		p = 1;
	//verific daca codificarea mesajului se termina cu id-ul postasului
	if(cod_mesaj >= id_cartier)
	{
		r = (cod_mesaj - id_cartier) % (int)pow(10, p);
	}
	else
		r = 1;
	if(r == 0)
		return 1;
	else 
		return 0;
}	

int alterare(int post_id_cartier)
{
	int i = 0, p, v[100], d = 2, msk = 0, j;

	if(post_id_cartier == 0)
	{
		v[0] = 0;
		i = 1;
	}
	if(post_id_cartier == 1)
	{
		v[0] = 1;
		i = 1;
	}
// gasesc factorii primi din descompunerea id-ului postasului

	while (post_id_cartier > 1)
	{
		p = 0;
		while(post_id_cartier % d == 0)
		{
			
			post_id_cartier = post_id_cartier / d;
			p = p + 1;
		}
		if(p)
		{ 	v[i] = d;
			i++;
		}
		d++;
	}

	//creez masca pentru inversarea bitilor
	for( j = 0; j < i; j++)
		if(v[j] <= 31)
			msk = msk | 1<<v[j];
	

	return msk;
}

void scor (postas *post, int nrC)
{
	int i, j, cor, k, k1, k2, k3;

	for(i = 0; i < nrC; i++)
	{
	
		cor = 0;
		for(j = 0; j < post[i].nr_pachete; j++)
		{	
			k = cod(post[i].v + j);

			// recalculez codificarea mesajului
			k1 = post[i].v[j].strada;
			k2 = post[i].v[j].numar;
			k3 = k % (k1 * k2);	
			//compar daca codificarea nou calculata este identica cu cea inregistrata anterior
			if(k3 == post[i].v[j].codificare_mesaj)
				cor++;
		}	

		printf("%d %f \n", post[i].id_cartier, (float)cor / post[i].nr_pachete);
	}
}

int main()
{ 
	int nrC, nrP, i, j, k, k1, k2;
	cartier c[100];
	pachet p[100];
	postas post[32];

	printf("Punctul 1\n");
	citire(c, p, &nrP, &nrC);

	printf("Punctul 2\n");
	adresa(p, &nrP);

	printf("Punctul 3\n");
	distribuire_pachete(c, p, post, &nrP, &nrC);

	printf("Punctul 4\n");
	comparare(post, p, &nrP, &nrC);

	printf("Punctul 5\n");
	for(i = 0; i < nrC; i++)
	{
		printf("%d %d ", c[i].id_cartier, post[i].nr_pachete);
		for(j = 0; j < post[i].nr_pachete; j++)	
		{
			k = cod(post[i].v + j);
			k1 = post[i].v[j].strada;
			k2 = post[i].v[j].numar;
			post[i].v[j].codificare_mesaj = k % (k1 * k2);//incarcare camp codificare_mesaj cu intreg

			printf("%d %d ", post[i].v[j].id_pachet, post[i].v[j].codificare_mesaj);
		}
		printf("\n");
	}

	printf("Punctul 6\n");
	for(i = 0; i < nrC; i++)
	{
		
		for(j = 0; j < post[i].nr_pachete; j++)	
			if(verificare(post[i].v[j].codificare_mesaj, post[i].id_cartier))
			{
				post[i].v[j].codificare_mesaj = post[i].v[j].codificare_mesaj ^ alterare(post[i].id_cartier);

			}
	}		
	
	for(i = 0; i < nrC; i++)
	{ 
		printf("%d %d ", c[i].id_cartier, post[i].nr_pachete); 

		for(j = 0; j < post[i].nr_pachete; j++)	
			printf("%d %d ", post[i].v[j].id_pachet, post[i].v[j].codificare_mesaj);	
		printf("\n");
	} 

	printf("Punctul 7\n");
	scor(post, nrC);

	return 0;
}
	





