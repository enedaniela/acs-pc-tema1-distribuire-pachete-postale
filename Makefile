CC = gcc
FLAGS = -Wall
PROGS = tema1
all: $(PROGS)

tema1: tema1.c
	$(CC) tema1.c -o tema1 $(FLAGS) -lm

clean:
	rm -f *~ *.out $(PROGS)
